﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjStudentManagement.Classes
{
    public class xHelper
    {
        public static int _pageRecordSize = 7;

        public static int PaginationCount(long counting)
        {
            decimal totalval = Convert.ToDecimal(counting) / xHelper._pageRecordSize;
            int orgval = Convert.ToInt32(counting / xHelper._pageRecordSize);

            if ((totalval % 1) > 0)
            {
                orgval = orgval + 1;
            }

            return orgval;
        }
    }
}