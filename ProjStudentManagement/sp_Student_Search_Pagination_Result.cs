//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjStudentManagement
{
    using System;
    
    public partial class sp_Student_Search_Pagination_Result
    {
        public Nullable<long> inRowNumber { get; set; }
        public int StudentId { get; set; }
        public string Name { get; set; }
        public System.DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public Nullable<long> inRecordCount { get; set; }
    }
}
