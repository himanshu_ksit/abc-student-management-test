﻿using ProjStudentManagement.Classes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjStudentManagement.Controllers
{
    public class HomeController : Controller
    {
        Interview_StudentMang_HimanshuEntities db = new Interview_StudentMang_HimanshuEntities();

        public ActionResult StudentManagement()
        {
            return View();
        }
        
        public ActionResult _studentList(string allpara = "", int pageno = 1)
        {
            // Student List get with with sp and Paginatio
            ViewBag.PageNo = pageno;
            int CourseId = 0;
            if (allpara != null && allpara != "")
            {
                CourseId = Convert.ToInt32(allpara);
            }
            var lresult = db.sp_Student_Search_Pagination(pageno, xHelper._pageRecordSize, CourseId).ToList();
            ViewBag.Resultcount = Convert.ToInt64(lresult.Select(c => c.inRecordCount).FirstOrDefault());
            ViewBag.Count = xHelper.PaginationCount(ViewBag.Resultcount);
            return PartialView(lresult);
        }

        public ActionResult addStudent()
        {
            ViewBag.courselst = db.Courses.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult addStudent(Student objstd, List<int> CourseId)
        {
            db.Students.Add(objstd);
            db.SaveChanges();
            // StudentCourse Table updated with Selected Course
            StudentCourse objstdcrs = new StudentCourse();
            foreach (var item in CourseId)
            {
                objstdcrs.CourseId = item;
                objstdcrs.StudentId = objstd.StudentId;
                db.StudentCourses.Add(objstdcrs);
                db.SaveChanges();
            }
            TempData["successMsg"] = "Student Add successfully !";
            return RedirectToAction("StudentManagement");
        }

        public ActionResult StudentDetail(int id)
        {
            var studnt = db.Students.Where(w => w.StudentId == id).FirstOrDefault();
            ViewBag.courselst = db.Courses.ToList();
            ViewBag.selectedcourse = db.StudentCourses.Where(w => w.StudentId == id).Select(w => w.CourseId).ToArray();
            return View(studnt);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StudentDetail(Student objStudent, List<int> CourseId)
        {
            var lstudent = db.Students.Where(w => w.StudentId == objStudent.StudentId).FirstOrDefault();
            lstudent.Name = objStudent.Name;
            lstudent.DateOfBirth = objStudent.DateOfBirth;
            lstudent.Gender = objStudent.Gender;
            db.Entry(lstudent).State = EntityState.Modified;
            db.SaveChanges();

            //Old Courses Deleted and Add New to avoid Duplication in Mapping
            StudentCourse objstdcrs = new StudentCourse();
            foreach (var item in CourseId)
            {
                var lqry = db.StudentCourses.Where(w => w.CourseId == item && w.StudentId == objStudent.StudentId).FirstOrDefault();
                if (lqry != null)
                {
                    db.Entry(lqry).State = EntityState.Deleted;
                    db.SaveChanges();
                }
            }
            foreach (var item in CourseId)
            {
                objstdcrs.CourseId = item;
                objstdcrs.StudentId = objStudent.StudentId;
                db.StudentCourses.Add(objstdcrs);
                db.SaveChanges();
            }
            TempData["successMsg"] = "Student Updated successfully !";
            return RedirectToAction("StudentManagement");
        }

        public ActionResult deleteStudent(int id)
        {
            // Delete Student with StudentCourse mapping Table
            var lstudent = db.Students.Where(w => w.StudentId == id).FirstOrDefault();
            db.Entry(lstudent).State = EntityState.Deleted;
            db.SaveChanges();

            var lcourse = db.StudentCourses.Where(w => w.StudentId == id).ToList();
            if (lcourse != null)
            {
                foreach (var item in lcourse)
                {
                    var lstcourse = db.StudentCourses.Where(w => w.StudentId == item.StudentId).FirstOrDefault();
                    db.Entry(lstcourse).State = EntityState.Deleted;
                    db.SaveChanges();
                }
            }
            TempData["successMsg"] = "Student Deleted successfully !";
            return RedirectToAction("StudentManagement");
        }
    
        public ActionResult courseLst()
        {
            return View();
        }

        public ActionResult _courseList(string allpara = "", int pageno = 1)
        {
            // Course List with sp and Pagination
            ViewBag.PageNo = pageno;
            int CourseId = 0;
            if (allpara != null && allpara != "")
            {
                CourseId = Convert.ToInt32(allpara);
            }
            var lresult = db.sp_Course_Search_Pagination(pageno, xHelper._pageRecordSize, CourseId).ToList();
            ViewBag.Resultcount = Convert.ToInt64(lresult.Select(c => c.inRecordCount).FirstOrDefault());
            ViewBag.Count = xHelper.PaginationCount(ViewBag.Resultcount);

            return PartialView(lresult);
        }

        public ActionResult addCourse()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult addCourse(Course objCourse)
        {
            db.Courses.Add(objCourse);
            db.SaveChanges();
            TempData["successMsg"] = "Course Add successfully !";
            return RedirectToAction("courseLst");
        }

        public JsonResult editCourse(int id)
        {
            var crse = db.Courses.Where(w => w.CourseId == id).FirstOrDefault();
            return Json(crse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult editCourse(Course obCourse)
        {
            var crse = db.Courses.Where(w => w.CourseId == obCourse.CourseId).FirstOrDefault();
            crse.CourseName = obCourse.CourseName;
            db.Entry(crse).State = EntityState.Modified;
            db.SaveChanges();
            TempData["successMsg"] = "Course Updated successfully !";
            return RedirectToAction("courseLst");
        }

        public ActionResult deleteCourse(int id)
        {
            //Prevent Course Delete if Course alredy use by Student
            var chkcourse = db.StudentCourses.Where(w => w.CourseId == id).FirstOrDefault();
            if (chkcourse == null)
            {
                var dltcrse = db.Courses.Where(w => w.CourseId == id).FirstOrDefault();
                db.Entry(dltcrse).State = EntityState.Deleted;
                db.SaveChanges();
                TempData["successMsg"] = "Course Deleted successfully !";
                return RedirectToAction("courseLst");
            }
            else
            {
                TempData["errorMsg"] = "Course Already in Use !";
                return RedirectToAction("courseLst");
            }
        }

        [HttpPost]
        public JsonResult search(string Prefix)
        {
            // Course Search filter
            if (!String.IsNullOrEmpty(Prefix))
            {
                var objList = db.Courses.Where(c => c.CourseName.Contains(Prefix)).ToList();
                return Json(objList, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult StudentSearch(string Prefix)
        {
            // Student Search filter
            if (!String.IsNullOrEmpty(Prefix))
            {
                var objList = db.Students.Where(c => c.Name.Contains(Prefix)).ToList();
                return Json(objList, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

    }
}